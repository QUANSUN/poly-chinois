'use strict';

angular
  .module('webApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller:  'MainCtrl'         
      })
      .when('/reg', {
        templateUrl: 'views/reg.html',
        controller:  'UserCtrl'         
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller:  'UserCtrl'         
      })
      .when('/article/:userId', {
        templateUrl: 'views/article.html',
        controller:  'ArticleCtrl'         
      })
      .when('/modif/:userId/:articleId', {
        templateUrl: 'views/modifier.html',
        controller:  'ModifCtrl'         
      })
      .when('/detail/:articleId', {
        templateUrl: 'views/detail.html',
        controller:  'DetailCtrl'         
      })
      .otherwise({
        redirectTo: '/'
      });
  });
