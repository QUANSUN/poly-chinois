'use strict';


angular.module('webApp')
  .controller('ArticleCtrl', ['$routeParams', '$location', '$scope', 'article', 'user', 'comment', function($routeParams, $location, $scope, article, user, comment) {
        $scope.newArticle = {
            'title' : null,
            'content':null,
            'UserId':null,
            'username':null,
            'image' : null,
        };
        $scope.data="";
        $scope.numArticle='';
        $scope.userInfo='';
        /**
         * This function is to test whether the text is null or contains the whitespace
         */
        function invalidateContent(text) {
            return text==null||text.replace(/\s/g, '').length < 1;
        }
        $scope.publish = function(title, content) {
            if(invalidateContent(title)) {
                window.alert("The title cannot be vide or whitespaces");
                return;
            } 
            if(invalidateContent(content)) {
                window.alert("The title cannot be vide or whitespaces");
                return;
            } 
            $scope.newArticle.content = content;
            $scope.newArticle.title = title;
            $scope.newArticle.UserId = $scope.userInfo.id;
            $scope.newArticle.username = $scope.userInfo.username;
            var x = Math.floor((Math.random() * 20) + 1);
            $scope.newArticle.image = 'article'+x+'.jpg';
            article.post($scope.newArticle, function(data) {
                $scope.mylist();
                }, function(error) {
                    console.log(error);
                });
        }
        $scope.mylist = function() {
            user.search($routeParams.userId+"/Articles",function(data) { 
                $scope.data = data;
                if(data==null||data.length==0) {
                    $scope.numArticle = 0;
                }else {
                    $scope.numArticle = data.length;
                }               
                }, function(error) {
                    console.log(error);
                });
        } 
        $scope.list = function() {
            article.all(function(data) { 
                $scope.data = data;              
                }, function(error) {
                    console.log(error);
                });
        }
        $scope.delete = function(articleId, UserId){
            if(UserId!=$routeParams.userId) {
                window.alert("This is not your article, you have no right to delete it!");
                return;
            } else {
                article.delete(articleId, function() {
                    $scope.mylist();
                    }, function(error) {
                        console.log(error);
                    });
            }
        } 

        
        $scope.update = function(articleId) {
            $location.path("/modif/"+$scope.userInfo.id+"/"+articleId);
        }
        $scope.init = function() {
            user.search($routeParams.userId, function(userData) {
                console.log(userData);
                if(userData.id==null) {
                    $location.path("/login");
                } else {
                    $scope.userInfo = userData;
                    $scope.mylist();
                }
            }, function(error) {
                console.log(error);
            });
            
        } 	
    }]);

