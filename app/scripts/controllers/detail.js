'use strict';


angular.module('webApp')
  .controller('DetailCtrl', ['$routeParams', '$location', '$scope', 'article', 'user', 'comment', function($routeParams, $location, $scope, article, user, comment) {
        $scope.data="";
        $scope.comments = '';
        /**
         * This function is to test whether the text is null or contains the whitespace
         */
        function invalidateContent(text) {
            return text==null||text.replace(/\s/g, '').length < 1;
        }

        $scope.comment= function(content) {
            if(invalidateContent(content)) {
                window.alert("Comment content cannot be vide or whitespace!");
                return;
            }
            $scope.newComment = {
                'content':content,
                'ArticleId':$routeParams.articleId,
                'username':"anonyme"
            };
            comment.post($scope.newComment, function(data) {
                 article.search($routeParams.articleId+"/Comments", function(data2) {
                        $scope.comments=data2;
                    }, function(error) {
                        console.log(error);
                    });
                 }, function(error) {
                    console.log(error);
                });
        }

        $scope.init = function() {
            console.log($routeParams.articleId);
            var articleId = $routeParams.articleId;
            article.search(articleId, function(data) {
                $scope.data=data;
                console.log(data);
            article.search(articleId+"/Comments", function(data2) {
                $scope.comments=data2;                
            article.search(articleId+"/Users", function(data2) {
                $scope.user=data2;
                user.search($scope.user.id+"/Articles", function(data2) {
                    if(!data2.length) {
                        $scope.numArticles = 0;
                    } else {
                       $scope.numArticles=data2.length;
                    }
                    }, function(error) {
                        console.log(error);
                    });
                }, function(error) {
                    console.log(error);
                });
            }, function(error) {
                console.log(error);
            });
            }, function(error) {
                console.log(error);
            });

        } 	
    }]);

