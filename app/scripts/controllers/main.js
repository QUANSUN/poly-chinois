'use strict';


angular.module('webApp')
  .controller('MainCtrl', ['$routeParams', '$location', '$scope', 'article', 'user', function($routeParams, $location, $scope, article, user) {
      $scope.articles = '';
      $scope.init = function() {
      article.all(function(data) {
                $scope.articles = data;
                }, function(error) {
                    console.log(error);
                });
          }
    }]);

