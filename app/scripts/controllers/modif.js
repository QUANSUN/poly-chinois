'use strict';


angular.module('webApp')
  .controller('ModifCtrl', ['$routeParams', '$location', '$scope', 'article', 'user', 'comment', function($routeParams, $location, $scope, article, user, comment) {
        $scope.newArticle = {
            'content':null,
            'UserId':null
        };
        $scope.data="";
        $scope.comments = '';
        /**
         * This function is to test whether the text is null or contains the whitespace
         */
        function invalidateContent(text) {
            return text==null||text.replace(/\s/g, '').length < 1;
        }

        $scope.modifier = function(articleId, userId) {
            if(userId != $routeParams.userId) {
                window.alert("This is not your article, you have no right to modify it!");
                return;
            }
            if(invalidateContent($scope.articleTitle)) {
                window.alert("The title cannot be vide or whitespace");
                return;
            }
            if(invalidateContent($scope.articleContent)) {
                window.alert("The content cannot be vide or whitespace");
                return;
            } 
            article.put(articleId, {'title' : $scope.articleTitle,'content' : $scope.articleContent}, function(data) {
                $location.path("/article/"+$routeParams.userId);
                }, function(error) {
                    console.log(error);
                });
        }

        $scope.cancel = function () {
            $location.path("/article/"+$routeParams.userId);
        }
        
        $scope.comment= function(content) {
            if(invalidateContent(content)) {
                window.alert("Comment content cannot be vide or whitespace!");
                return;
            }

            $scope.newComment = {
                'content':content,
                'ArticleId':$routeParams.articleId,
                'UserId' : $routeParams.userId,
                'username':''
            };

            user.search($routeParams.userId, function(userData) {
                $scope.newComment.username = userData.username;
            	comment.post($scope.newComment, function(data) {
                	article.search($routeParams.articleId+"/Comments", function(data2) {
                    	$scope.comments=data2;
                	}, function(error) {
                    	console.log(error);
                	});
                	}, function(error) {
                    	console.log(error);
               		 });
            		}, function(error) {
                    	console.log(error);
                	});
        }

        $scope.init = function() {
            var articleId = $routeParams.articleId;      
            article.search($routeParams.articleId, function(data) {
                $scope.currentArticle=data;
                $scope.articleTitle = data.title;
                $scope.articleContent = data.content;
            article.search($routeParams.articleId+"/Comments", function(data2) {
                $scope.comments=data2;
                
            user.search(data.UserId, function(userData) {
                $scope.userInfo = userData;
            
            user.search(data.UserId+"/Articles",function(data3) { 
                if(data3==null||data3.length==0) {
                    $scope.numArticle = 0;
                } else {
                    $scope.numArticle = data3.length;
                }
                }, function(error) {
                    console.log(error);
                });
            }, function(error) {
                console.log(error);
            });
            }, function(error) {
                console.log(error);
            });
            }, function(error) {
                    console.log(error);
           });

        } 	
    }]);
