'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('webApp')
  .controller('UserCtrl', ['$location', '$scope', 'user', function($location, $scope, user) {
  		$scope.us='';
  		$scope.userId = 2;
  		$scope.userInfo = {
  			'username' : null,
  			'email' : null,
  			'pwd' :null,
            'status':0,
            'type':0
  		};
        $scope.users = "";
        /**
         * This function creates a pattern for validating a compte of email
         */ 
        function validateEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }
        /**
         * This function is to test whether the text is null or contains the whitespace
         */
        function invalidateUsernameOrPwd(text) {
            return text==null||text.replace(/\s/g, '').length < 1 || /\s/.test(text);
        }
        /**
         * This function is to sign up a compte of an user, if the  username or email or pwd is not correct,
         * it will print the error in the page by using javacript to change the content of the page.
         */
        $scope.signUp = function(username, email, pwd) {
            $scope.userInfo.username = username;
            $scope.userInfo.email = email;
            $scope.userInfo.pwd = pwd;
            if(invalidateUsernameOrPwd(username)) {
                window.alert("*Username cannot be vide or contains whitespace");
                return;
            }
             
            if(!validateEmail(email)) {
            	return;
            }
            var flag = true;
            user.all(function(data) {
            	$scope.users = data;
            	
                angular.forEach($scope.users, function(value) {
                    if(flag) {
                        if(value.email==email) {
                            flag=false;
                            return;     
                        }
                    }
                });
                if(!flag) {
                    window.alert("The email already exists!");
                    return;
                } else {
                	if(invalidateUsernameOrPwd(pwd)) {
		                window.alert("*Password cannot be vide or contains whitespace");
		                return;
		            } else {
		            	user.post($scope.userInfo, function(data) {
			                    $location.path('/login');
			            }, function(error) {
			                console.log(error);
			                return;
			            });
		            }
                }
            	}, function(error) {
            		console.log(error);
            	});           
        }

        /**
         * This function is to log in the website!
         */
        $scope.login = function(email,pwd) {
            if(!validateEmail(email)) {
              return;
            }
            user.get(email, pwd, function(data) {
               if(data!=null) {
               	$location.path('/article/'+data.id);              	
               } else {
               	window.alert("The email or Password is incorrect!");
               }
            }, function(error){
                console.log(error);
            });
        }
    	$scope.get=function(userId) { 		
    		user.get($scope.userId, function(userData) {
    			$scope.us = userData;
    		}, function(error) {
    			console.log(error);
    		});
    	}

    	$scope.delete = function(userId) {
    		users.delete(2, function(data) {
    			window.alert(data);
    		}, function(error) {
    			console.log(error);
    		});
    	}
    	
    }]);

