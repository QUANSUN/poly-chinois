'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('webApp')
	.factory("article", ['$http', function($http) {
  		var url = "http://localhost:3000/api/Articles/";
      var obj = {
  			get:function(email, pwd, successCB, failCB) { 
   				 $http.get(url)
    				.success(function(data) {
              if(data.status=='success') {
                var flag = true;
                angular.forEach(data.data, function(value) {
                  if(flag) {
                  if(value.email==email&&value.pwd==pwd) {
                     successCB(value);
                     flag=false;
                  }
                }
                });
             } 
    				})
    				.error(function(error){
    					failCB(error);
    				});
  			},

  			delete:function(articleId, successCB, failCB) {
  				$http.delete(url+articleId)
  				.success(function() {
              successCB();
  				})
  				.error(function(error) {
              failCB(error);
  				});
  			},
  			post:function(data, successCB, failCB) {
  				$http.post(url, data)
  				.success(function(data) {
              if(data.status=='success') {
                successCB(data.data);
              }
  				})
  				.error(function(error) {
              failCB(error);
  				})
  			},
  			put:function(articleId, data, successCB, failCB) {
  				$http.put(url+articleId, data)
  				.success(function(data2) {
            console.log(data2);
              successCB("Success update");
  				})
  				.error(function(error) {
              failCB(error);
  				})
  			},
        search:function(articleId, successCB, failCB) {
          $http.get(url+articleId)
          .success(function(data) {
            if(data.status=='success') {
                successCB(data.data);
            }
          })
          .error(function(error) {
              failCB(error);
          })
        },
  			all:function(successCB, errorCB) {
  				$http.get(url)
  				.success(function(data) {
            if(data.status=='success') {
  				  	successCB(data.data);
          }
  				})
  				.error(function(error) {
  					errorCB(error);
  				})
  			}
  		};
  		return obj;
  	}]);
